let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

let renderGlasses = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let content = `
    <img class="vglasses__items" onclick="selectGlasses('${item.id}')" style="width: 30%; height: 60%" src="${item.src}" alt="hinhKinh"> 
    `;
    contentHTML += content;
  });

  document.getElementById("vglassesList").innerHTML = contentHTML;
};
renderGlasses(dataGlasses);

let locationSearch = (index, list) => {
  return list.findIndex((item) => {
    return item.id == index;
  });
};

let selectGlasses = (id) => {
  let index = locationSearch(id, dataGlasses);

  if (index != -1) {
    let item = dataGlasses[index];
    let VGlassesHTML = `<img id="VGlasses" src="${item.virtualImg}" alt="hinhKinh">`;

    let glassesInfoHTML = `
    <h5>${item.name} - ${item.brand} (${item.color})</h5>
    <p>
    <span style ="background: red; color: White; padding: 4px; border-radius: 3px; margin-right: 5px ">$${item.price}</span>
    <span style="color: green">Stocking</span>
    </p>
    <p>${item.description}</p>
    `;

    document.getElementById("avatar").innerHTML = VGlassesHTML;
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("glassesInfo").innerHTML = glassesInfoHTML;
  }
};

let removeGlasses = (isGlasses) => {
  if (isGlasses) {
    document.getElementById("VGlasses").style.display = "block";
  } else {
    document.getElementById("VGlasses").style.display = "none";
  }
};
